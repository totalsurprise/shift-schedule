package de.totalsurprise.shiftschedule

import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api")
class ShiftScheduleRestController {

    @GetMapping("/users/{id}/teams")
    fun getTeamsForUser(
            @PathVariable(name = "id") userId: String,
            @RequestHeader("Authorization") token: String
    ): List<TeamEto>{
        println(token)
        return listOf(
                TeamEto("1", "Das große Team"),
                TeamEto("2", "Das kleine Team")
        )
    }

    @GetMapping("/teams/{id}/members")
    fun getMembersForTeam(
            @PathVariable(name = "id") teamId: String
    ): List<MemberEto> {
        return if (teamId == "1") listOf(
                MemberEto("1", "Werner"),
                MemberEto("2", "Eckart"),
                MemberEto("3", "Meister Röhrich"))
        else emptyList()
    }

}

data class TeamEto(
        val id: String,
        val teamname: String
)

data class MemberEto(
        val id: String,
        val nickname: String
)