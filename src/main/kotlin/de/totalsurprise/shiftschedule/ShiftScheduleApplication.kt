package de.totalsurprise.shiftschedule

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShiftScheduleApplication

fun main(args: Array<String>) {
    runApplication<ShiftScheduleApplication>(*args)
}
