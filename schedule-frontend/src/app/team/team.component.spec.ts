import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TeamComponent} from './team.component';
import {TeamService} from "../team.service";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs/internal/observable/of";
import {Team} from "../team";

describe('TeamComponent', () => {
  const teamsTestData = [
    new Team('123', 'Team Name 1'),
    new Team('234', 'Team Name 2')
  ];

  let component: TeamComponent;
  let fixture: ComponentFixture<TeamComponent>;
  let compiled;

  beforeEach(async(() => {
    let mockTeamService = {
      getTeamsForCurrentUser: function () {
        return of(teamsTestData);
      }
    };

    TestBed.configureTestingModule({
      declarations: [TeamComponent],
      imports: [
        RouterTestingModule
      ],
      providers: [
        {provide: TeamService, useValue: mockTeamService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the heading ´Your Teams´', () => {
    expect(compiled.querySelector('h2').textContent).toContain('Your Teams:');
  });

  it('should render one team card per team', () => {
    let listItems = compiled.querySelectorAll('.card-list li');
    expect(listItems.length).toEqual(teamsTestData.length);

    let firstCardHeading = listItems[0].querySelector('h3');
    expect(firstCardHeading.textContent).toContain('Team Name 1');

    let firstCardRouterLink = listItems[0].querySelector('a');
    expect(firstCardRouterLink.textContent).toContain('Members');
    expect(firstCardRouterLink.getAttribute('href'))
      .toEqual('/teams/123/members');
  });
});
