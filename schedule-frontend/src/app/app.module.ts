import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {TeamComponent} from './team/team.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';
import {MemberComponent} from './member/member.component';
import {AppRoutingModule} from './app-routing.module';
import {environment} from "../environments/environment";
import {AuthServiceConfig, GoogleLoginProvider, SocialLoginModule} from "angular-6-social-login";
import {TokenInterceptor} from "./http-interceptor";

@NgModule({
  declarations: [
    AppComponent,
    TeamComponent,
    MemberComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SocialLoginModule,
    environment.production ? [] : InMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: false}
    ),
    AppRoutingModule
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs
  }, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}

// Configs
export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("793470707093-ni9r9j0teo8rpkf0hrmoeehrhnvhu1pc.apps.googleusercontent.com")
      }
    ]);
}
