import {inject, TestBed} from '@angular/core/testing';

import {TeamService} from './team.service';
import {HttpClient} from "@angular/common/http";
import {Team} from "./team";
import {of} from "rxjs/internal/observable/of";

describe('TeamService', () => {

  let httpClient: HttpClient;
  let service: TeamService;

  beforeEach(() => {
    const mockHttpClient = {
      get: function () {
        return of(
          [new Team('123', 'Team Name 1'),
          new Team('234', 'Team Name 2')]
        );
      }
    } as any;

    TestBed.configureTestingModule({
      providers: [
        TeamService,
        {provide: HttpClient, useValue: mockHttpClient}
      ]
    });
    service = TestBed.get(TeamService);

    httpClient = TestBed.get(HttpClient);
  });

  it('should be created', inject([TeamService], (service: TeamService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getTeamsForCurrentUser', () => {

    it('should return an Observable<Team[]>', (done) => {
      // when
      const result = service.getTeamsForCurrentUser();

      // then
      result.subscribe(teams => {
        expect(teams.length).toEqual(2);
        done();
      });
    });

    it('should call GET api/teams', (done) => {
      // given
      spyOn(httpClient, "get").and.callThrough();

      // when
      service.getTeamsForCurrentUser().subscribe(() => {

        // then
        expect(httpClient.get).toHaveBeenCalledWith('api/teams');
        done();
      });

    });

  });
});
