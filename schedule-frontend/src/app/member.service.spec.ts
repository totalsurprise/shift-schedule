import {inject, TestBed} from '@angular/core/testing';

import {MemberService} from './member.service';
import {HttpClient} from "@angular/common/http";

describe('MemberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MemberService,
        {provide: HttpClient, useValue: {}}
      ]
    });
  });

  it('should be created', inject([MemberService], (service: MemberService) => {
    expect(service).toBeTruthy();
  }));
});
