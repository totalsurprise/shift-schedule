import {Injectable} from '@angular/core';
import {Observable} from "rxjs/internal/Observable";
import {Team} from "./team";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(
    private http: HttpClient
  ) {

  }

  getTeamsForCurrentUser(): Observable<Team[]> {
    const userId = localStorage.getItem('userId');
    let url = 'api/users/' + userId + '/teams';
    return this.http.get<Team[]>(url);
  }

}
