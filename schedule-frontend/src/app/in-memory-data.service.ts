import {Injectable} from '@angular/core';
import {getStatusText, InMemoryDbService, RequestInfo, ResponseOptions, STATUS} from "angular-in-memory-web-api";

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() {
  }

  private members = [
    {id: '1', nickname: 'Kunibert'},
    {id: '2', nickname: 'Friedhelm'},
    {id: '3', nickname: 'Sir Stone'},
    {id: '4', nickname: 'Alfonso'},
    {id: '5', nickname: 'Bruce Wayne'},
    {id: '6', nickname: 'Mrs. T'},
    {id: '7', nickname: 'OhneRotkohl'},
    {id: '8', nickname: 'Lustiges Lieschen'},
    {id: '8', nickname: 'Bob'}
  ];

  private teams = [
    {id: 'abc', teamname: 'Die Schnullis', members: this.members},
    {id: 'bcd', teamname: 'Team17', members: []},
    {id: 'dgj', teamname: 'Alle meine Entchen', members: []},
    {id: 'gsh', teamname: 'Santa\'s Six', members: []},
    {id: 'gsj', teamname: 'Knallfrösche', members: []},
  ];


  createDb() {
    return {
      teams: this.teams,
      members: this.members
    };
  }

  get(requestInfo: RequestInfo) {

    let body = this.getBody(requestInfo);

    return requestInfo.utils.createResponse$(() => {
      let response: ResponseOptions = {
        body: body,
        status: STATUS.OK,
        headers: requestInfo.headers,
        url: requestInfo.url
      };
      response.statusText = getStatusText(response.status);
      return response;
    });
  }

  private getBody(requestInfo: RequestInfo) {
    let regExTeams = new RegExp('api\\/users\\/.*\\/teams\\/?');
    let regExTeamMembers = new RegExp('api\\/teams\\/.*\\/members\\/?');

    if (regExTeams.test(requestInfo.url)) {
      return this.teams;
    } else if (regExTeamMembers) {
      let team = requestInfo.utils.findById(this.teams, requestInfo.id);
      return team.members;
    } else {
      return null;
    }
  }
}
