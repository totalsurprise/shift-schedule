import {Injectable} from '@angular/core';
import {Observable} from "rxjs/internal/Observable";
import {Member} from "./member";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(
    private http: HttpClient
  ) { }

  getMembers(teamId: string): Observable<Member[]> {
    console.log('Fetching members for team ' + teamId);
    let url = 'api/teams/' + teamId + '/members/';
    let response = this.http.get<Member[]>(url);
    return response;
  }
}
