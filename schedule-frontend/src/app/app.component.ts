import {Component} from '@angular/core';
import {AuthService, GoogleLoginProvider} from 'angular-6-social-login';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public user;

  constructor(private socialAuthService: AuthService){}

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(userData);

        this.user = userData;
        localStorage.setItem('userId', userData.id);
        localStorage.setItem('token', userData.idToken);
      }
    )
  }

  public socialSignOut() {
    this.socialAuthService.signOut().then(
      () => {
        this.user = null;
        localStorage.setItem('userId', null);
        localStorage.setItem('token', null);
      }
    )
  }
}
